/*
 * TestService.java
 */

package com.nomadconnection.apprtc;

import android.content.Context;
import android.opengl.EGLContext;
import android.util.Log;

import com.nomadconnection.apprtc.AppRTCClient.SignalingParameters;
import com.nomadconnection.apprtc.util.LooperExecutor;
import org.webrtc.DataChannel;
import org.webrtc.IceCandidate;
import org.webrtc.Logging;
import org.webrtc.MediaCodecVideoEncoder;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaConstraints.KeyValuePair;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnection.IceConnectionState;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.StatsObserver;
import org.webrtc.StatsReport;

import org.webrtc.VideoCapturerAndroid;
import org.webrtc.VideoRenderer;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.util.EnumSet;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Peer connection client implementation.
 *
 * <p>All public methods are routed to local looper thread.
 * All PeerConnectionEvents callbacks are invoked from the same looper thread.
 * This class is a singleton.
 */
public class TestService extends Service implements 
        AppRTCClient.Si{

  private static final String TAG = "TestService";

  private boolean isError = false;
  private boolean isInitiator = true;
  private static final String AUDIO_CODEC_OPUS = "opus";
  private static final String AUDIO_CODEC_ISAC = "ISAC";

  private static final TestService instance = new TestService();
  private final PCObserver pcObserver = new PCObserver();
  private final SDPObserver sdpObserver = new SDPObserver();
  private final LooperExecutor executor;

  private PeerConnectionFactory factory;
  private PeerConnection peerConnection;
  PeerConnectionFactory.Options options = null;
  //private PeerConnectionEvents events;

  private SessionDescription localSdp; // either offer or answer SDP
  private MediaStream mediaStream;
  private LinkedList<IceCandidate> queuedRemoteCandidates;

  private VideoCapturerAndroid videoCapturer;
  // enableVideo is set to true if video should be rendered and sent.
  private VideoTrack localVideoTrack;
  private VideoTrack remoteVideoTrack;

  /**
   * Peer connection parameters.
   */

  private TestService() {
    executor = new LooperExecutor();
    // Looper thread is started once in private ctor and is used for all
    // peer connection API calls to ensure new peer connection factory is
    // created on the same thread as previously destroyed factory.
    executor.requestStart();
  }

  /**
   * Peer connection events.
  public static interface PeerConnectionEvents {
    public void onLocalDescription(final SessionDescription sdp);
    public void onIceCandidate(final IceCandidate candidate);
    public void onIceConnected();
    public void onIceDisconnected();
    public void onPeerConnectionClosed();
    public void onPeerConnectionStatsReady(final StatsReport[] reports);
    public void onPeerConnectionError(final String description);
  }
   */

  public static TestService getInstance() {
    return instance;
  }

  public void setPeerConnectionFactoryOptions(PeerConnectionFactory.Options options) {
    this.options = options;
  }

  private void createPeerConnectionFactory() {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        if (peerConnectionClient == null) {
          final long delta = System.currentTimeMillis() - callStartedTimeMs;
          peerConnectionClient = PeerConnectionClient.getInstance();
          peerConnectionClient.createPeerConnectionFactory(CallActivity.this,
              VideoRendererGui.getEGLContext(), peerConnectionParameters,
              CallActivity.this);
        }
      }
    });
  }

  public void createPeerConnection() {
  }



  // Implementation detail: observe ICE & stream changes and react accordingly.
  private class PCObserver implements PeerConnection.Observer {
    @Override
    public void onIceCandidate(final IceCandidate candidate){
      executor.execute(new Runnable() {
        @Override
        public void run() {
          //events.onIceCandidate(candidate);
        }
      });
    }

    @Override
    public void onSignalingChange(
        PeerConnection.SignalingState newState) {
    }

    @Override
    public void onIceConnectionChange(
        final PeerConnection.IceConnectionState newState) {
      executor.execute(new Runnable() {
        @Override
        public void run() {
          Log.d(TAG, "IceConnectionState: " + newState);
          if (newState == IceConnectionState.CONNECTED) {
            //events.onIceConnected();
          } else if (newState == IceConnectionState.DISCONNECTED) {
            //events.onIceDisconnected();
          } else if (newState == IceConnectionState.FAILED) {
            //reportError("ICE connection failed.");
          }
        }
      });
    }

    @Override
    public void onIceGatheringChange(
      PeerConnection.IceGatheringState newState) {
    }

    @Override
    public void onAddStream(final MediaStream stream){
      executor.execute(new Runnable() {
        @Override
        public void run() {
          if (peerConnection == null || isError) {
            return;
          }
          if (stream.audioTracks.size() > 1 || stream.videoTracks.size() > 1) {
            //reportError("Weird-looking stream: " + stream);
            return;
          }
          if (stream.videoTracks.size() == 1) {
            //remoteVideoTrack = stream.videoTracks.get(0);
            //remoteVideoTrack.setEnabled(renderVideo);
            //remoteVideoTrack.addRenderer(new VideoRenderer(remoteRender));
          }
        }
      });
    }

    @Override
    public void onRemoveStream(final MediaStream stream){
      executor.execute(new Runnable() {
        @Override
        public void run() {
          if (peerConnection == null || isError) {
            return;
          }
          remoteVideoTrack = null;
          //stream.videoTracks.get(0).dispose();
        }
      });
    }

    @Override
    public void onDataChannel(final DataChannel dc) {
    }

    @Override
    public void onRenegotiationNeeded() {
      // No need to do anything; AppRTC follows a pre-agreed-upon
      // signaling/negotiation protocol.
    }
  } // PCObserver

  private void drainCandidates() {
    if (queuedRemoteCandidates != null) {
      for (IceCandidate candidate : queuedRemoteCandidates) {
        peerConnection.addIceCandidate(candidate);
      }
      queuedRemoteCandidates = null;
    }
  }

  // Implementation detail: handle offer creation/signaling and answer setting,
  // as well as adding remote ICE candidates once the answer SDP is set.
  private class SDPObserver implements SdpObserver {
    @Override
    public void onCreateSuccess(final SessionDescription origSdp) {
      if (localSdp != null) {
        //reportError("Multiple SDP create.");
        return;
      }
      String sdpDescription = origSdp.description;
      //sdpDescription = preferCodec(sdpDescription, AUDIO_CODEC_ISAC, true);

      final SessionDescription sdp = new SessionDescription(origSdp.type, sdpDescription);
      localSdp = sdp;
      executor.execute(new Runnable() {
        @Override
        public void run() {
          if (peerConnection != null && !isError) {
            Log.d(TAG, "Set local SDP from " + sdp.type);
            peerConnection.setLocalDescription(sdpObserver, sdp);
          }
        }
      });
    }

    @Override
    public void onSetSuccess() {
      executor.execute(new Runnable() {
        @Override
        public void run() {
          if (peerConnection == null) {
            return;
          }

          if (isInitiator) {
            if (peerConnection.getRemoteDescription() == null) {
              //events.onLocalDescription(localSdp);
            } else {
              drainCandidates();
            }
          } else {
            if (peerConnection.getLocalDescription() != null) {
              //events.onLocalDescription(localSdp);
              drainCandidates();
            } else {
              // We've just set remote SDP - do nothing for now -
              // answer will be created soon.
            }
          }
        }
      });
    }

    @Override
    public void onCreateFailure(final String error) {
      //reportError("createSDP error: " + error);
    }

    @Override
    public void onSetFailure(final String error) {
      //reportError("setSDP error: " + error);
    }
  } // SDP Observer
}
