package com.nomad.handofftest;

import android.util.Log;

import com.github.nkzawa.socketio.client.Socket;

import com.nomad.handofftest.socket.io.SocketIOClient;
import com.nomad.handofftest.util.JsonUtils;
import com.nomad.handofftest.util.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.PeerConnection;
import org.webrtc.SessionDescription;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by goldworm on 2015-03-18.
 */
public class SignalingRTCClient implements AppRTCClient, SocketIOClient.Listener {
    private static final String TAG = Config.TAG;
    private String myId;
    private SocketIOClient socketIoClient;
    private boolean loopback = false;
    private SignalingEvents events;

    public SignalingRTCClient(SignalingEvents events) {
        this.events = events;
    }

    public void sendTo(JSONObject json, String to) {
        if (myId != null) {
            JsonUtils.jsonPut(json, "from", myId);
        }

        if (to != null) {
            JsonUtils.jsonPut(json, "to", to);
        }

        socketIoClient.send(json);
    }

    private static String getEventHandlerMethodName(String type) {
        if (type == null || type.isEmpty()) {
            return null;
        }

        return "on" + StringUtils.capitalize(type);
    }

    @Override
    public void connectToRoom(String url, boolean loopback) {
        Log.d(TAG, "connectToRoom()");

        socketIoClient = new SocketIOClient(url, this);
        this.loopback = loopback;
        socketIoClient.connect();
    }

    @Override
    public void sendOfferSdp(String to, SessionDescription sdp) {
        Log.d(TAG, "SignalingRTCClient.sendOfferSdp()");

        sendSdp("offer", to, sdp);

        if (loopback) {
            // In loopback mode rename this offer to answer and route it back.
            SessionDescription sdpAnswer = new SessionDescription(
                    SessionDescription.Type.fromCanonicalForm("answer"),
                    sdp.description);
            events.onRemoteDescription(sdpAnswer);
        }
    }

    @Override
    public void sendAnswerSdp(String to, SessionDescription sdp) {
        Log.d(TAG, "SignalingRTCClient.sendAnswerSdp()");
        sendSdp("answer", to, sdp);
    }

    private void sendSdp(String type, String to, SessionDescription sdp) {
        JSONObject json = new JSONObject();
        JsonUtils.jsonPut(json, "sdp", sdp.description);
        JsonUtils.jsonPut(json, "type", type);

        sendTo(json, to);
    }

    @Override
    public void sendLocalIceCandidate(String to, IceCandidate candidate) {
        Log.d(TAG, "SignalingRTCClient.sendLocalIceCandidate()");

        JSONObject json = new JSONObject();
        JsonUtils.jsonPut(json, "type", "candidate");
        JsonUtils.jsonPut(json, "label", candidate.sdpMLineIndex);
        JsonUtils.jsonPut(json, "id", candidate.sdpMid);
        JsonUtils.jsonPut(json, "candidate", candidate.sdp);

        if (loopback) {
            if (events != null) {
                events.onRemoteIceCandidate(candidate);
            }
        }
        else {
            sendTo(json, to);
        }
    }

    @Override
    public void disconnectFromRoom() {
        Log.d(TAG, "SignalingRTCClient.disconnectFromRoom()");

        if (socketIoClient != null) {
            socketIoClient.disconnect();
        }
    }

    // -------------------------------------------------------------------------
    // Implementation of SocketIOClient.Listener interface
    // -------------------------------------------------------------------------

    @Override
    public void onConnect(Socket socket, Object... args) {
        Log.d(TAG, "SignalingRTCClient.onConnect()");
        // ToDo
        join(Config.SERVICE_KEY, Config.ROOM, Config.DEVICE, Config.NAME);
    }

    public void join(String serviceKey, String room, String device, String name) {
        Log.d(TAG, "SignalingRTCClient.join()");

        try {
            JSONObject obj = new JSONObject();
            obj.put("type", "join");
            obj.put("device", device);
            obj.put("name", name);
            obj.put("room", room);
            obj.put("serviceKey", serviceKey);

            socketIoClient.send(obj);
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    /**
     * Signaling server와의 연결이 끊김
     * @param socket
     * @param args
     */
    @Override
    public void onDisconnect(Socket socket, Object... args) {
        Log.d(TAG, "SignalingRTCClient.onDisconnect()");
        events.onChannelClose();
    }

    @Override
    public void onMessage(Socket socket, Object... args) {
        try {
            JSONObject json = (JSONObject)args[0];
            Log.d(TAG, "SignalingRTCClient.onMessage() type:" + json.getString("type"));

            final String type = json.getString("type");
            switch (type) {
                case "candidate":
                    onCandidate(json);
                    break;

                case "offer":
                    onOffer(json);
                    break;

                case "answer":
                    onAnswer(json);
                    break;

                case "out":
                    // A peer is disconnected from signaling server.
                    break;

                default:
                    // callback method name protocol
                    // ex) in -> onIn(), acceptCall -> onAcceptCall()
                    handleSignal(json);
                    break;
            }
        } catch (JSONException je) {
            je.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Execute AppRTCClient's event handler whose name is derived from a message's type.
     * ex) acceptCall -> onAcceptCall()
     * @param json
     * @throws JSONException
     */
    private void handleSignal(final JSONObject json) throws JSONException {
        String type = json.getString("type");
        String methodName = getEventHandlerMethodName(type);
        if (methodName == null) {
            return;
        }

        try {
            Method method = events.getClass().getMethod(methodName, getClass(), json.getClass());
            method.invoke(events, this, json);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void hi(String device, String name) {
        Log.d(TAG, "SignalingRTCClient.hi()");

        try {
            JSONObject json = new JSONObject();
            json.put("type", "hi");
            json.put("device", device);
            json.put("name", name);

            sendTo(json, null);
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    private void onCandidate(JSONObject json) throws JSONException {
        Log.d(TAG, "SignalingRTCClient.onCandidate()");
        if (events != null) {
            IceCandidate candidate = new IceCandidate(
                    json.getString("id"),
                    json.getInt("label"),
                    json.getString("candidate"));
            events.onRemoteIceCandidate(candidate);
        }
    }

    private void onOffer(JSONObject json) throws JSONException {
        Log.d(TAG, "SignalingRTCClient.onOffer()");
        if (events != null) {
            SessionDescription sdp = new SessionDescription(
                    SessionDescription.Type.fromCanonicalForm("offer"),
                    json.getString("sdp"));
            events.onRemoteDescription(sdp);
        }
    }

    private void onAnswer(JSONObject json) throws JSONException {
        Log.d(TAG, "SignalingRTCClient.onAnswer()");
        if (events != null) {
            SessionDescription sdp = new SessionDescription(
                    SessionDescription.Type.fromCanonicalForm("answer"),
                    json.getString("sdp"));
            events.onRemoteDescription(sdp);
        }
    }

    public void welcome(String to, String device, String name) {
        Log.d(TAG, "SignalingRTCClient.welcome()");

        try {
            JSONObject json = new JSONObject();
            json.put("type", "welcome");
            json.put("device", device);
            json.put("name", name);

            sendTo(json, to);
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void callAccepted(String by, String cid) {
        JSONObject json = new JSONObject();
        JsonUtils.jsonPut(json, "type", "callAccepted");
        JsonUtils.jsonPut(json, "cid", cid);
        JsonUtils.jsonPut(json, "from", myId);
        JsonUtils.jsonPut(json, "by", by);

        socketIoClient.send(json);
    }

    public void callDeclined(String to, String cid) {
        sendCallRelatedResponse("callDeclined", to, cid);
    }

    public void callEnded(String to, String cid) {
        sendCallRelatedResponse("callEnded", to, cid);
    }

    public void callChanged(String to, String cid) {
        sendCallRelatedResponse("callChanged", to, cid);
    }

    public void callMade(String to, String cid) {
        sendCallRelatedResponse("callMade", to, cid);
    }

    public void lineBusy(String to, String cid) {
        sendCallRelatedResponse("lineBusy", to, cid);
    }

    private void sendCallRelatedResponse(String type, String to, String cid) {
        JSONObject json = new JSONObject();
        JsonUtils.jsonPut(json, "type", type);
        JsonUtils.jsonPut(json, "cid", cid);
        JsonUtils.jsonPut(json, "from", myId);
        JsonUtils.jsonPut(json, "to", to);

        socketIoClient.send(json);
    }

    public void smSent(String to, String smid) {
        JSONObject json = new JSONObject();
        JsonUtils.jsonPut(json, "type", "smSent");
        JsonUtils.jsonPut(json, "smid", smid);
        JsonUtils.jsonPut(json, "from", myId);
        JsonUtils.jsonPut(json, "to", to);

        socketIoClient.send(json);
    }

    public void resContact(String to, Object contacts) {
        JSONObject json = new JSONObject();
        JsonUtils.jsonPut(json, "type", "resContact");
        JsonUtils.jsonPut(json, "from", myId);
        JsonUtils.jsonPut(json, "to", to);

        if (contacts != null) {
            JsonUtils.jsonPut(json, "contacts", contacts);
        }

        socketIoClient.send(json);
    }
}
