package com.nomad.handofftest;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.IBinder;

//import android.support.v7.app.ActionBarActivity;

import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nomad.handofftest.util.LogUtils;
import com.nomad.handofftest.util.TelephonyUtils;

import java.util.ArrayList;


public class MainActivity extends Activity implements View.OnClickListener {
    private static final String TAG = Config.TAG;
    private EditText callEditText;

    private EditText smsEditText;
    private EditText callEditText2;

    private boolean isServiceBound;
    private HandOffService handOffService;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            handOffService = ((HandOffService.LocalBinder)service).getService();

            // Tell the user about this for our demo.
            Toast.makeText(MainActivity.this, "service is connected.",
                    Toast.LENGTH_SHORT).show();
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            handOffService = null;
            Toast.makeText(MainActivity.this, "service is disconnected.",
                    Toast.LENGTH_SHORT).show();
        }
    };

    private MMSManager mmsManager = new MMSManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final int buttonIds[] = {
                R.id.offer_button,
                R.id.hangup_button,
                R.id.call_recv_button,
                R.id.call_ended_button,
                R.id.sm_recv_button,
        };

        for (int i = 0; i < buttonIds.length; i++) {
            ((Button) this.findViewById(buttonIds[i])).setOnClickListener(this);
        }

        ((Button) this.findViewById(R.id.service_start_button)).setOnClickListener(this);
        ((Button) this.findViewById(R.id.service_stop_button)).setOnClickListener(this);

        // UI to make a phone call
        callEditText = (EditText) this.findViewById(R.id.call_editText);
        // The NomadConnection second phone number for HomeNetwork business.
        callEditText.setText("07042421575");
        ((Button) this.findViewById(R.id.call_button)).setOnClickListener(this);

        // UI for SMS
        callEditText2 = (EditText) this.findViewById(R.id.call_editText2);
        callEditText2.setText("07042421575");
        smsEditText = (EditText) this.findViewById(R.id.sms_editText);
        smsEditText.setText("가나다라마바사아자차카타파하.가나다라마바사아자차카타파하.가나다라마바사아자차카타파하.가나다라마바사아자차카타파하.가나다라마바사아자차카타파하.가나다라마바사아자차카타파하.가나다라마바사아자차카타파하.가나다라마바사아자차카타파하.가나다라마바사아자차카타파하.가나다라마바사아자차카타파하.가나다라마바사아자차카타파하.가나다라마바사아자차카타파하.");
        ((Button)this.findViewById(R.id.sms_button)).setOnClickListener(this);
        ((Button)this.findViewById(R.id.mtm_button)).setOnClickListener(this);
        ((Button)this.findViewById(R.id.mms_button)).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy()");

        unbindHandOffService();

        super.onDestroy();
    }

    private void unbindHandOffService() {
        if (isServiceBound) {
            unbindService(serviceConnection);
            isServiceBound = false;
        }
    }

    @Override
    public void onClick(View v) {
        final int id = v.getId();

        String buttonName = ((Button) v).getText().toString();
        LogUtils.logAndToast(this, TAG, buttonName + " is clicked.");

        switch (id) {
            case R.id.service_start_button:
                bindHandOffService();
                break;

            case R.id.service_stop_button: {
                unbindHandOffService();
                Intent intent = new Intent(this, HandOffService.class);
                stopService(intent);
                break;
            }

            case R.id.call_button:
                call();
                break;

            case R.id.sms_button:
                sendShortMessage(0);
                break;

            case R.id.mtm_button:
                sendShortMessage(1);
                break;

            case R.id.mms_button:
                sendShortMessage(2);
                break;

            case R.id.offer_button:
                offer();
                break;

            case R.id.hangup_button:
                hangup();
                break;

            case R.id.call_recv_button:
                fireCallRecvEvent();
                break;

            case R.id.call_ended_button:
                fireCallEndedEvent();
                break;

            case R.id.sm_recv_button:
                fireSmRecvEvent();
                break;
        }
    }

    private void bindHandOffService() {
        Intent intent = new Intent(this, HandOffService.class);
        intent.setData(Uri.parse(Config.SIGNAL_SERVER_URL));

        startService(intent);
        bindService(intent, serviceConnection, 0);
        isServiceBound = true;
    }

    private void call() {
        String phoneNumber = callEditText.getText().toString().trim();
        String url = "tel:" + phoneNumber;
        LogUtils.logAndToast(this, TAG, url);

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
        startActivity(intent);
    }

    private void sendShortMessage(int type) {
        String phoneNumber = callEditText2.getText().toString().trim();
        String msg = smsEditText.getText().toString().trim();

        switch (type) {
            case 0: // sms
                if (msg.length() > 40) {
                    msg = msg.substring(0, 40);
                }
                sendSMS(phoneNumber, msg);
                break;

            case 1:
                sendMTM(phoneNumber, msg);
                break;

            case 2:
                sendMMS(phoneNumber, msg);
                break;
        }
    }

    private void sendSMS(String smsNumber, String smsText){
        PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT_ACTION"), 0);
        PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED_ACTION"), 0);

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        // 전송 성공
                        Toast.makeText(MainActivity.this, "전송 완료", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        // 전송 실패
                        Toast.makeText(MainActivity.this, "전송 실패", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        // 서비스 지역 아님
                        Toast.makeText(MainActivity.this, "서비스 지역이 아닙니다", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        // 무선 꺼짐
                        Toast.makeText(MainActivity.this, "무선(Radio)가 꺼져있습니다", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        // PDU 실패
                        Toast.makeText(MainActivity.this, "PDU Null", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter("SMS_SENT_ACTION"));

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        // 도착 완료
                        Toast.makeText(MainActivity.this, "SMS 도착 완료", Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        // 도착 안됨
                        Toast.makeText(MainActivity.this, "SMS 도착 실패", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter("SMS_DELIVERED_ACTION"));

        SmsManager mSmsManager = SmsManager.getDefault();
        mSmsManager.sendTextMessage(smsNumber, null, smsText, sentIntent, deliveredIntent);
        Toast.makeText(this, " sms 전송되었습니다.", Toast.LENGTH_SHORT).show();
    }

    private void sendMTM(String phoneNumber, String message) {
        SmsManager smsManager = SmsManager.getDefault();
        String sendTo = phoneNumber;
//        String from = "01040422217";
        String from = TelephonyUtils.getMyPhoneNumber(this);
        ArrayList<String> partMessage = smsManager.divideMessage(message);

        smsManager.sendMultipartTextMessage(sendTo, from, partMessage, null, null);
        Toast.makeText(this, from + " mtm 전송되었습니다.", Toast.LENGTH_SHORT).show();
    }

    private void sendMMS(String phoneNumber, String message) {
        mmsManager.sendMMS(this, phoneNumber, message);
        Toast.makeText(this, " mms 전송되었습니다.", Toast.LENGTH_SHORT).show();
    }

    /**
     * webrtc offer command
     */
    private void offer() {
        if (handOffService != null) {
            handOffService.offer();
        }
    }

    /**
     * Disconnect webrtc communication
     */
    private void hangup() {
        handOffService.hangup();
    }

    /**
     * smRecv event simulation for test
     */
    private void fireSmRecvEvent() {
        if (handOffService != null) {
            handOffService.fireSmRecv();
        }
    }

    /**
     * callRecv event simulation for test
     */
    private void fireCallRecvEvent() {
        if (handOffService != null) {
            handOffService.fireCallRecv();
        }
    }

    /**
     * callEnded event simulation for test
     */
    private void fireCallEndedEvent() {
        if (handOffService != null) {
            handOffService.fireCallEnded();
        }
    }
}
