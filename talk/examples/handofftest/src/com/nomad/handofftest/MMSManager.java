package com.nomad.handofftest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.provider.ContactsContract;

import com.klinker.android.send_message.Message;
import com.klinker.android.send_message.Settings;
import com.klinker.android.send_message.Transaction;

/**
 * Implement MMS features
 * Created by goldworm on 2015-06-09.
 */
final public class MMSManager {
    // SKT 3G
    final String MMSC = "http://omms.nate.com:9082/oma_mms";
    final String MMS_PROXY = "smart.nate.com";

    // SKT LTE
    final String LTE_MMS_PROXY = "lteoma.nate.com";

    private Settings sendSettings = new Settings();

    public MMSManager() {
        init();
    }

    private void init() {
        sendSettings.setMmsc(MMSC);
        sendSettings.setProxy(LTE_MMS_PROXY);
        sendSettings.setPort("9093");
        sendSettings.setGroup(true);
        sendSettings.setDeliveryReports(false);
        sendSettings.setSplit(false);
        sendSettings.setSplitCounter(false);
        sendSettings.setStripUnicode(false);
        sendSettings.setSignature("");
        sendSettings.setSendLongAsMms(true);
        sendSettings.setSendLongAsMmsAfter(1);
    }

    public final void sendMMS(Context context, String phoneNumber, String msg) {
        sendSettings.setProxy(LTE_MMS_PROXY);
        sendSettings.setPort("9093");

        Transaction sendTransaction = new Transaction(context, sendSettings);

        Message message = new Message(msg, phoneNumber);
//        mMessage.setImage(mBitmap);   // not necessary for voice or sms messages
        message.setType(Message.TYPE_SMSMMS);  // could also be Message.TYPE_VOICE
        sendTransaction.sendNewMessage(message, 0);
    }

    public final void sendSMS(Context context, String phoneNumber, String msg) {
        sendSettings.setProxy(null);
        sendSettings.setPort("80");

        Transaction sendTransaction = new Transaction(context, sendSettings);

        Message message = new Message(msg, phoneNumber);
//        mMessage.setImage(mBitmap);   // not necessary for voice or sms messages
        message.setType(Message.TYPE_SMSMMS);  // could also be Message.TYPE_VOICE
        sendTransaction.sendNewMessage(message, 0);
    }
}
