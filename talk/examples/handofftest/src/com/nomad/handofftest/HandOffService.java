package com.nomad.handofftest;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;

import com.nomad.handofftest.util.JsonUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.PeerConnection;
import org.webrtc.SessionDescription;

import java.util.LinkedList;
import java.util.List;

/**
 * Service managing a signaling channel.
 * Created by goldworm on 2015-04-01.
 */
public class HandOffService extends Service implements
        AppRTCClient.SignalingEvents,
        PeerConnectionClient.PeerConnectionEvents {
    private static final String TAG = Config.TAG;
    private Handler handler = new Handler();
    private SignalingRTCClient signalingClient;
    private AppRTCAudioManager audioManager;
    private PeerConnectionClient pc;
    private boolean isCaller;
    private boolean isPeerConnectionReady;
    private String myId;
    private String peerId;
    AppRTCClient.SignalingParameters signalingParameters;

    public class LocalBinder extends Binder {
        public HandOffService getService() {
            return HandOffService.this;
        }
    }
    private final IBinder binder = new LocalBinder();

    private PhoneCallReceiver phoneCallReceiver = new PhoneCallReceiver();
    private ContactObserver contactObserver = new ContactObserver(handler);

    @Override
    public void onCreate() {
        Log.d(TAG, "HandOffService.onCreate()");
        super.onCreate();

        isCaller = false;
        isPeerConnectionReady = false;
        initSignalingRTCClient();
        initPeerConnectionClient();
        initPhoneCallReceiver();
        initContactObserver();
    }

    private void initSignalingRTCClient() {
        signalingClient = new SignalingRTCClient(this);
    }

    private void initAudioManager() {
        // Create and audio manager that will take care of audio routing,
        // audio modes, audio device enumeration etc.
        audioManager = AppRTCAudioManager.create(this, new Runnable() {
                    // This method will be called each time the audio state (number and
                    // type of devices) has been changed.
                    @Override
                    public void run() {
                        onAudioManagerChangedState();
                    }
                }
        );

        // Store existing audio settings and change audio mode to
        // MODE_IN_COMMUNICATION for best possible VoIP performance.
        Log.d(TAG, "Initializing the audio manager...");
        audioManager.init();
    }

    private void deinitAudioManager() {
        if (audioManager != null) {
            audioManager.close();
            audioManager = null;
        }
    }

    private void initPeerConnectionClient() {
        if (pc == null) {
            pc = new PeerConnectionClient();
        }
    }

    private void initPhoneCallReceiver() {
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_NEW_OUTGOING_CALL);
        this.registerReceiver(phoneCallReceiver, intentFilter);

        phoneCallReceiver.start(this);
    }

    private void initContactObserver() {
        getContentResolver().registerContentObserver(
                ContactsContract.RawContacts.CONTENT_URI, false, contactObserver);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "HandOffService.onStartCommand()");

        Uri uri = intent.getData();
        signalingClient.connectToRoom(uri.toString(), false);

        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "HandOffService.onBind()");
        return binder;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "HandOffService.onDestroy()");
        if (signalingClient != null) {
            signalingClient.disconnectFromRoom();
        }

        deinitContactObserver();
        deinitPhoneCallReceiver();
        deinitPeerConnectionClient();
        deinitAudioManager();

        super.onDestroy();
    }

    private void deinitPhoneCallReceiver() {
        phoneCallReceiver.stop(this);
        this.unregisterReceiver(phoneCallReceiver);
    }

    private void deinitContactObserver() {
        getContentResolver().unregisterContentObserver(contactObserver);
    }

    private void deinitPeerConnectionClient() {
        closePeerConnection();
        pc = null;
    }

    private void onAudioManagerChangedState() {
        Log.d(TAG, "HandOffService.onAudioManagerChangedState()");
    }

    // -------------------------------------------------------------------------
    // Implementation of PeerConnectionClient.PeerConnectionEvents interface
    // -------------------------------------------------------------------------

    @Override
    public void onLocalDescription(final SessionDescription sdp) {
        Log.d(TAG, "HandOffService.onLocalDescription()");

        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Sending " + sdp.type + " ...");
                if (isCaller) {
                    signalingClient.sendOfferSdp(peerId, sdp);
                } else {
                    signalingClient.sendAnswerSdp(peerId, sdp);
                }
            }
        });
    }

    @Override
    public void onIceCandidate(final IceCandidate candidate) {
        Log.d(TAG, "HandOffService.onIceCandidate()");

        handler.post(new Runnable() {
            @Override
            public void run() {
                if (signalingClient != null) {
                    signalingClient.sendLocalIceCandidate(peerId, candidate);
                }
            }
        });
    }

    @Override
    public void onIceConnected() {
        Log.d(TAG, "HandOffService.onIceConnected()");
    }

    @Override
    public void onIceDisconnected() {
        Log.d(TAG, "HandOffService.onIceDisconnected()");
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                Log.d(TAG, "ICE disconnected");
//                disconnect();
//            }
//        });
    }

    /**
     * onPeerConnectionClosed() is called in PeerConnectionClient.closeInternal().
     */
    @Override
    public void onPeerConnectionClosed() {
        Log.d(TAG, "HandOffService.onPeerConnectionClosed()");
    }

    @Override
    public void onPeerConnectionError(String description) {
        Log.d(TAG, "HandOffService.onPeerConnectionError()");
        Log.d(TAG, description);

//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                if (!isError) {
//                    isError = true;
//                    disconnectWithErrorMessage(description);
//                }
//            }
//        });
    }

    // -------------------------------------------------------------------------
    // Implementation of AppRTCClient.SignalingEvents interface
    // -------------------------------------------------------------------------
    /**
     *
     * @param client
     * @param json
     */
    @Override
    public void onIn(SignalingRTCClient client, final JSONObject json) {
        Log.d(TAG, "HandOffService.onIn()");

        try {
            myId = json.getString("id");
            signalingParameters = createSignalingParameters(json);

            client.hi(Config.DEVICE, Config.NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Receive a sdp from a remote peer.
     * @param sdp Session Description Protocol
     */
    @Override
    public void onRemoteDescription(final SessionDescription sdp) {
        Log.d(TAG, "HandOffService.onRemoteDescription()");

        handler.post(new Runnable() {
            @Override
            public void run() {
                if (pc == null) {
                    return;
                }

                Log.d(TAG, "Received remote " + sdp.type + "...");
                pc.setRemoteDescription(sdp);

                if (!isCaller) {
                    Log.d(TAG, "Creating ANSWER...");
                    // Create answer. Answer SDP will be sent to offering client in
                    // PeerConnectionEvents.onLocalDescription event.
                    pc.createAnswer();
                }
            }
        });
    }

    @Override
    public void onRemoteIceCandidate(final IceCandidate candidate) {
        Log.d(TAG, "HandOffService.onRemoteIceCandidate()");

        handler.post(new Runnable() {
            @Override
            public void run() {
                if (pc != null) {
                    pc.addRemoteIceCandidate(candidate);
                }
            }
        });
    }

    /**
     * This app is disconnected from signaling server.
     */
    @Override
    public void onChannelClose() {
        Log.d(TAG, "HandOffService.onChannelClose()");
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                Log.d(TAG, "Remote end hung up; dropping PeerConnection");
//                if (pc != null) {
//                    pc.close();
//                    pc = null;
//                }
//                if (audioManager != null) {
//                    audioManager.close();
//                    audioManager = null;
//                }
//            }
//        });
    }

    @Override
    public void onChannelError(String description) {
        Log.d(TAG, "HandOffService.onChannelError()");
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                if (!isError) {
//                    isError = true;
//                    disconnectWithErrorMessage(description);
//                }
//            }
//        });
    }

    /**
     * A peer leaved from the room. (not myself)
     * @param client
     * @param json
     */
    public void onOut(SignalingRTCClient client, final JSONObject json) {
        Log.d(TAG, "HandOffService.onOut()");
    }

    /*
     * FIXME
     * It is used just for testing.
     */
    private AppRTCClient.SignalingParameters createSignalingParameters(JSONObject json) {
        String clientId = json.optString("id");
        String rid = json.optString("rid");
        List<PeerConnection.IceServer> iceServers = new LinkedList<PeerConnection.IceServer>();
        MediaConstraints videoConstraints = null;
        MediaConstraints audioConstraints = new MediaConstraints();
        MediaConstraints pcConstraints = new MediaConstraints();
        pcConstraints.optional.add(new MediaConstraints.KeyValuePair("DtlsSrtpKeyAgreement", "true"));

        return new AppRTCClient.SignalingParameters(
                iceServers, true, pcConstraints, videoConstraints, audioConstraints, rid,
                clientId, null, null, null, null);
    }

    public void onHi(SignalingRTCClient client, final JSONObject json) {
        Log.d(TAG, "HandOffService.onHi()");

        try {
            String to = json.getString("from");
            client.welcome(to, Config.DEVICE, Config.NAME);
            this.peerId = to;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onWelcome(SignalingRTCClient client, final JSONObject json) {
        Log.d(TAG, "HandOffService.onWelcome()");
        try {
            String to = json.getString("from");
            this.peerId = to;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onAcceptCall(SignalingRTCClient client, final JSONObject json) {
        Log.d(TAG, "HandOffService.onAcceptCall()");

        try {
            String to = json.getString("from");
            String cid = json.getString("cid");
            client.callAccepted(to, cid);

            handler.post(new Runnable() {
                @Override
                public void run() {
                    offer();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * agent -> phone
     * 착신 전화를 거절한다. (전화를 받지 않음)
     * @param json
     */
    public void onDeclineCall(SignalingRTCClient client, final JSONObject json) {
        Log.d(TAG, "HandOffService.onDeclineCall()");

        try {
            String to = json.getString("from");
            String cid = json.getString("cid");
            client.callDeclined(to, cid);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * agent -> phone
     * 현재 통화를 종료한다.
     * @param json
     */
    public void onEndCall(SignalingRTCClient client, final JSONObject json) {
        Log.d(TAG, "HandOffService.onEndCall()");

        try {
            String to = json.getString("from");
            String cid = json.getString("cid");
            client.callEnded(to, cid);

            handler.post(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "Remote end hung up; dropping PeerConnection");
                    closePeerConnection();
                    isCaller = false;
//                    if (audioManager != null) {
//                        audioManager.close();
//                        audioManager = null;
//                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closePeerConnection() {
        if (isPeerConnectionReady) {
            if (pc != null) {
                pc.close();
            }
            isPeerConnectionReady = false;
        }
    }

    /**
     * agent -> phone
     * 주소록을 요청한다.
     * @param json
     */
    public void onReqContact(SignalingRTCClient client, final JSONObject json) {
        Log.d(TAG, "HandOffService.onReqContact()");

        try {
            String to = json.getString("from");
            JSONArray contacts = getContacts();
            client.resContact(to, contacts);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private JSONArray getContacts() {
        final String[][] values = {
                {"홍길동", "010-1111-1111"},
                {"김삿갓", "010-1234-5678"},
                {"고길동", "010-9999-8888"},
        };

        JSONArray contacts = new JSONArray();

        try {
            for (int i = 0; i < values.length; i++) {
                JSONObject contact = new JSONObject();
                contact.put("name", values[i][0]);
                contact.put("pn", values[i][1]);
                contacts.put(contact);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return contacts;
    }

    /**
     * agent -> phone
     * 전화 발신을 요청한다.
     * @param json
     */
    public void onMakeCall(SignalingRTCClient client, final JSONObject json) {
        Log.d(TAG, "HandOffService.onMakeCall()");

        try {
            String to = json.getString("from");
            String cid = json.getString("cid");
            client.callMade(to, cid);

            handler.post(new Runnable() {
                @Override
                public void run() {
                    offer();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onSendSM(SignalingRTCClient client, final JSONObject json) {
        Log.d(TAG, "HandOffService.onSendSM()");

        try {
            String to = json.getString("from");
            String smid = json.getString("smid");
            client.smSent(to, smid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // -------------------------------------------------------------------------
    // public methods for test
    // -------------------------------------------------------------------------

    /**
     * phone -> agent
     * webrtc sdp offer
     * webrtc 연결 요청
     * It should be called in UI thread.
     */
    public void offer() {
        if (pc != null) {
            isCaller = true;
            initAudioManager();
            openPeerConnection();
            pc.createOffer();
        }
    }

    private void openPeerConnection() {
        if (!isPeerConnectionReady) {
            pc.createPeerConnectionFactory(this, false, null, this);

            final int startBitrate = 0;
            pc.createPeerConnection(null, null, signalingParameters, startBitrate);
            isPeerConnectionReady = true;
        }
    }

    /**
     * Disconnect the current peer connection.
     */
    public void hangup() {
        closePeerConnection();
        deinitAudioManager();
        isCaller = false;
    }

    /**
     * phone -> agents
     * 전화 착신 이벤트 알림
     */
    public void fireCallRecv() {
        Log.d(TAG, "HandOffService.fireCallRecv()");

        JSONObject json = new JSONObject();
        JsonUtils.jsonPut(json, "type", "callRecv");
        JsonUtils.jsonPut(json, "senderName", "홍길동");
        JsonUtils.jsonPut(json, "senderPN", "010-1234-5678");
        JsonUtils.jsonPut(json, "cid", "0");

        signalingClient.sendTo(json, null);
    }

    /**
     * phone -> agent
     * 상대방이 전화를 종료했다는 이벤트를 agent에게 알림
     */
    public void fireCallEnded() {
        Log.d(TAG, "HandOffService.fireCallEnded()");

        String to = peerId;
        String cid = "0";

        signalingClient.callEnded(to, cid);
    }

    /**
     * phone -> agents
     * SMS 메세지 수신을 agent에 알림
     */
    public void fireSmRecv() {
        Log.d(TAG, "HandOffService.fireSmRecv()");

        JSONObject json = new JSONObject();
        JsonUtils.jsonPut(json, "type", "smRecv");
        JsonUtils.jsonPut(json, "smid", "0");
        JsonUtils.jsonPut(json, "senderName", "버락 오바마");
        JsonUtils.jsonPut(json, "senderPN", "010-7777-8888");
        JsonUtils.jsonPut(json, "text", "시간날 때 백악관에 놀러오세요.");

        signalingClient.sendTo(json, null);
    }

    /**
     * phone -> agent
     * Agent로 통화 중 Phone으로 통화기기 변경
     */
    public void fireCallChanged(String to, String cid) {
        Log.d(TAG, "HandOffService.fireCallChanged()");
        signalingClient.callChanged(to, cid);
    }

    /**
     * phone -> agent
     * 현재 통화 중임을 알림
     * 한 agent가 전화 발신(makeCall)을 요청했을 때 발생할 수 있음
     */
    public void fireLineBusy(String to, String cid) {
        Log.d(TAG, "HandOffService.fireLineBusy()");
        signalingClient.lineBusy(to, cid);
    }

    /**
     * phone -> agent
     * agent에서 전송한 전화 발신 요청을 수락했음을 알림
     */
    public void fireCallMade(String to, String cid) {
        Log.d(TAG, "HandOffService.fireCallMade()");
        signalingClient.callMade(to, cid);
    }
}
