package com.nomad.handofftest;

import android.database.ContentObserver;

import android.os.Handler;
import android.util.Log;

/**
 * Created by goldworm on 2015-03-25.
 */
public class ContactObserver extends ContentObserver {
    private static final String TAG = "handoff";

    public ContactObserver(Handler handler) {
        super(handler);
    }

    @Override
    public boolean deliverSelfNotifications() {
        return false;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        Log.d(TAG, "ContactObserver.onChange() " + selfChange);
    }
}
