package com.nomad.handofftest.socket.io;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

/**
 * Socket.io client for java
 * Created by goldworm on 2015-03-17.
 */
public class SocketIOClient {
    private Socket socket;
    private Listener listener;

    /**
     * Socke.IO related listener
     */
    public interface Listener {
        /**
         * signaling server와 연결이 됨
         * @param socket
         * @param args
         */
        public void onConnect(Socket socket, Object... args);

        /**
         * signaling server와의 네트웍 연결이 끊김
         * @param socket
         * @param args
         */
        public void onDisconnect(Socket socket, Object... args);

        /**
         * signaling server로 부터 메세지가 수신됨
         * @param socket
         * @param args
         */
        public void onMessage(Socket socket, Object... args);
    }

    public SocketIOClient(String uri, Listener listener) {
        try {
            socket = IO.socket(uri);
        } catch(URISyntaxException use) {
        }

        this.listener = listener;
        socket.off();

        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Listener listener = SocketIOClient.this.listener;
                if (listener != null) {
                    listener.onConnect(socket, args);
                }
            }
        }).on("message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Listener listener = SocketIOClient.this.listener;
                if (listener != null) {
                    listener.onMessage(socket, args);
                }
            }
        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Listener listener = SocketIOClient.this.listener;
                if (listener != null) {
                    listener.onDisconnect(socket, args);
                }
            }
        });
    }

    public void connect() {
        this.socket.connect();
    }

    public void send(String message) {
        this.socket.send(message);
    }

    public void send(Object obj) {
        this.socket.send(obj);
    }

    public void disconnect() {
        this.socket.disconnect();
        this.socket = null;
    }
}
