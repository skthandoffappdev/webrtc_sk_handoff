package com.nomad.handofftest;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.util.Log;
import android.os.Handler;

/*
import com.nomad.call.PhoneCallReceiver;
import com.nomad.contact.ContactObserver;
*/

/**
 * Created by goldworm on 2015-03-24.
 */
public class SimpleHandOffService extends Service {
    private static final String TAG = "handoff";
    private PhoneCallReceiver phoneCallReceiver = new PhoneCallReceiver();
    private ContactObserver contactObserver = new ContactObserver(new Handler());

    public SimpleHandOffService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "HandOffService.onStartCommand()");

        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_NEW_OUTGOING_CALL);
        this.registerReceiver(phoneCallReceiver, intentFilter);

        phoneCallReceiver.start(this);
        registerContactObserver();

        return super.onStartCommand(intent, flags, startId);
    }

    private void registerContactObserver() {
        getContentResolver().registerContentObserver(
                ContactsContract.RawContacts.CONTENT_URI, false, contactObserver);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "HandOffService.onDestroy()");
        unregisterContactObserver();
        phoneCallReceiver.stop(this);
        this.unregisterReceiver(phoneCallReceiver);
        super.onDestroy();
    }

    private void unregisterContactObserver() {
        getContentResolver().unregisterContentObserver(contactObserver);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Not supporting binding
        return null;
    }
}
