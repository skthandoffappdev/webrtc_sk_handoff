package com.nomad.handofftest;

/**
 * Created by goldworm on 2015-03-31.
 */
public final class Config {
    private Config() {}

    public static final String TAG = "handoff";
    public static final boolean USE_APP_RTC = false;
    public static final String SIGNAL_SERVER_URL = "http://192.168.11.52:1106";
//    public static final String SIGNAL_SERVER_URL = "http://192.168.11.126:3000";

    public static final String SERVICE_KEY = "handoff";
    public static final String ROOM = "handoff";
    public static final String DEVICE = "phone";
    public static final String NAME = "vega";
}
