package com.nomad.handofftest.util;

/**
 * Created by goldworm on 2015-04-02.
 */
public final class StringUtils {
    private StringUtils() {}

    public static String capitalize(String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return str;
        }
        return new StringBuilder(strLen)
                .append(Character.toTitleCase(str.charAt(0)))
                .append(str.substring(1))
                .toString();
    }
}
