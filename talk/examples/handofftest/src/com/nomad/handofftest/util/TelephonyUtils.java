package com.nomad.handofftest.util;

import android.content.Context;
import android.telephony.TelephonyManager;

/**
 * Created by goldworm on 2015-06-09.
 */
final public class TelephonyUtils {
    public static String getMyPhoneNumber(Context context) {

        String PhoneNumber = "";

        try {
            TelephonyManager systemService =
                (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            PhoneNumber = systemService.getLine1Number();

            PhoneNumber = PhoneNumber.substring(PhoneNumber.length()-10,PhoneNumber.length());
            PhoneNumber="0"+PhoneNumber;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return PhoneNumber;
    }
}
