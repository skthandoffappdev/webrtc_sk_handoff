package com.nomad.handofftest.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by goldworm on 2015-04-01.
 */
public final class LogUtils {
    private static Toast logToast;

    private LogUtils() {}

    public static void logAndToast(Context context, String tag, String msg) {
        Log.d(tag, msg);

        if (logToast != null) {
            logToast.cancel();
        }

        logToast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        logToast.show();
    }
}
