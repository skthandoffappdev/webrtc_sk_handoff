package com.nomad.handofftest.util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by goldworm on 2015-03-31.
 */
public final class JsonUtils {
    private JsonUtils() {}

    public static void jsonPut(JSONObject json, String key, Object value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
}
