package com.nomad.handofftest.test;

import android.test.InstrumentationTestCase;
import android.util.Log;

import com.nomad.handofftest.AppRTCClient;
import com.nomad.handofftest.SignalingRTCClient;
import org.webrtc.IceCandidate;
import org.webrtc.SessionDescription;

/**
 * Created by goldworm on 2015-03-19.
 */
/*
public class ExampleTest extends InstrumentationTestCase
        implements AppRTCClient.SignalingEvents {
    private static final String TAG = "handoff";

    public void testSignalingRTCClient() throws Exception {
        final String uri = "http://192.168.11.77:1106";

        SignalingRTCClient client = new SignalingRTCClient(this);

        client.connectToRoom(uri, false);

        synchronized (this) {
            wait();
        }

        client.disconnectFromRoom();
    }

    @Override
    public void onConnectedToRoom(AppRTCClient.SignalingParameters params) {
        Log.d(TAG, "onConnectedToRoom()");
        synchronized (this) {
            notify();
        }
    }

    @Override
    public void onRemoteDescription(SessionDescription sdp) {
        Log.d(TAG, "onRemoteDescription()");
    }

    @Override
    public void onRemoteIceCandidate(IceCandidate candidate) {
        Log.d(TAG, "onRemoteIceCandidate()");
    }

    @Override
    public void onChannelClose() {
        Log.d(TAG, "onChannelClose()");
    }

    @Override
    public void onChannelError(String description) {
        Log.d(TAG, "onChannelError()");
    }
}
*/