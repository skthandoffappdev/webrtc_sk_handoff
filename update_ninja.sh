#!/bin/bash

# To update out_ninja.tgz for sharing
echo "find out -name \"*.ninja\" > ninja_update_list.txt"
find out -name "*.ninja" > ninja_update_list.tmp
sort ninja_update_list.tmp > ninja_update_list.txt
rm ninja_update_list.tmp

echo "tar cfz out_ninja.tgz \`cat ninja_update_list.txt\`"
tar cfz out_ninja.tgz `cat ninja_update_list.txt`
