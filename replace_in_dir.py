#!/usr/bin/python
# To replace a string of files in a directory & sub-directories.
import os
import sys
import re

file_list = "";
from_str = "";
to_str = "";

file_list_name = "";
tmp_file_name = "tmp_file_for_replace";

######################################
# Functions
######################################
def replace_a_file( filename ) :
	global tmp_file_name, from_str, to_str
	filename = filename.strip();
	print '\nTo check:', filename;

	fd = open(filename.strip());
	lines = fd.readlines();

	tmp_file = open(tmp_file_name, 'w+');
	# replace a string in a file
	for oneLine in lines:
		if from_str in oneLine:
			newLine = oneLine.replace(from_str, to_str);
			tmp_file.write(newLine);
			print("from:", oneLine);
			print("to:", newLine);
		else:
			tmp_file.write(oneLine);
		#end if
	#end for, oneLine

	# close files
	tmp_file.close();
	fd.close();

	os.rename(tmp_file_name, filename);
#end, replace_a_file

######################################
# Main Proc.
######################################
if len(sys.argv) == 4:
	folder = sys.argv[1];
	from_str = sys.argv[2];
	to_str = sys.argv[3];
	print("search dir: %s, from: %s, to %s" % (sys.argv[1], sys.argv[2], sys.argv[3]));
else:
	print ("Usage: %s <search dir> <from> <to> " % sys.argv[0]);
	exit(0);

# Traverse files
for root, dirs, filenames in os.walk(folder):
	for filename in filenames:
		# filepath is relatively given based on "folder".
		# if you want to see full path, you can use os.path.join(root, filename)
		filepath = os.path.join(root, filename);
		replace_a_file(filepath.strip());
	#end for, filenames
#end for, os.walk
